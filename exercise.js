// Теоретичні питання

// 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
  
  // Рекурсія - це виклик функцією самої себе з іншим зазвичай зміненим початковим параметром.



//     Завдання

/*Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому 
Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера число, яке введе користувач.
За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
Використовувати синтаксис ES6 для роботи зі змінними та функціями.
Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, 
або при введенні вказав не число, - запитати число заново (при цьому значенням для нього 
повинна бути введена раніше інформація).*/



let a = parseInt(prompt("Введіть число:"));

function factorial(a) {
  if (a > 0) {
    let result = a * factorial(a - 1);
    return result;
  
  } else if (a === 0) {
    return 1;
  }
}

if (isNaN(a)) {
  a = Number(prompt("Введіть число ще раз:", a));
} else if (a < 0) {
  console.log(`Факторіал числа ${a} невизначений`);
} else {factorial};


console.log(factorial(a));


